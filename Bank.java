package week5.BankSystem;

import java.util.Scanner;

class InvalidAmountException extends Exception{
    public InvalidAmountException() {
        System.out.println(" withdraw and deposit can not be zero or negative ");
    }
}
class InsufficientBalanceException extends Exception{
    public InsufficientBalanceException() {
        System.out.println("InsufficientBalanceException");
    }
}
interface Bank {

    void deposit();
    void withdraw();
}
class BankOperation implements Bank{
    static int balance=0;
    static Scanner sc=new Scanner(System.in);
    @Override
    public void deposit() {
        System.out.println("enter the amount of deposit");
        int addMoney=sc.nextInt();
        if(addMoney<0){
            try{
                throw new InvalidAmountException();
            } catch (InvalidAmountException e) {
                System.out.println(e);
            }
        }else {
            balance = balance + addMoney;
            System.out.println("balance after deposit is " + balance);
        }

    }

    @Override
    public void withdraw() {
        System.out.println("enter the value to be withdraw");
        int withdrawMoney=sc.nextInt();
        if(withdrawMoney<0){
            try{
                throw new InvalidAmountException();
            } catch (InvalidAmountException e) {
                System.out.println(e);
            }
        }
        if(withdrawMoney>balance){
            try {
                throw new InsufficientBalanceException();
            } catch (InsufficientBalanceException e) {
                System.out.println(e);
            }
        }

        balance=balance-withdrawMoney;
        System.out.println("balance  after withdraw is "+balance);

    }
}
